/*
*User Authentication table
*/
CREATE TABLE tbl_user(userId VARCHAR(50) PRIMARY KEY,
                      username VARCHAR(50),
		      password VARCHAR(50),
		      createTime DATETIME,
		      modifiedTime DATETIME);
/*
*User Details table
*/
CREATE TABLE tbl_user_details(userId VARCHAR(50) PRIMARY KEY,
                      phonenumber VARCHAR(15),
		      address VARCHAR(500),
		      pincode VARCHAR(6),
		      emailid  VARCHAR(50),
		      createTime DATETIME,
		      modifiedTime DATETIME);


/*
* Product table
*/
CREATE TABLE tbl_product(productId VARCHAR(50),
                      name VARCHAR(100),
		      description VARCHAR(1000),
		      type VARCHAR(20),
		      createTime DATETIME,
		      modifiedTime DATETIME);